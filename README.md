# Clean Architecture TodoApp

## Benoit de Cachard

---

### Lancer l’application

Pour lancer le projet il suffit de faire les commande “npm install” et ensuite “ionic serve”

---

### Architectures et choix

J’ai structuré le projet en domain / infra / presenters

Mon entité Task à comme propriétés : id,description,date(creation) et completed

Les cas d’utilisations CreateTask, GetTasks, UpdateTask, et DeleteTask ont été implémentés mais,  DeleteTask n’est pas actuellement utilisé dans l’application

J’ai implémenté deux adapteurs pour stocker les données, un va stocker les données dans le localStorage du navigateur et l’autre stocke dans IndexedDB du navigateur. 

Pour modifier quel système de stockage on veut utiliser il suffit d’aller dans presenters/pages/Home.tsx et dans la déclaration de taskService de choisir LocalStorageAdapter ou  IndexedDBAdapter

---

### Utilisation

Pour l’utilisateur il est possible de créer une tache, il peut ensuite modifier la description de cette tache et cocher une case pour la marquer comme “completed”.

Chacune de ces actions fait appel a un cas d’utilisation qui va ensuite utiliser le système de données choisi.