import { IonItem, IonInput, InputChangeEventDetail, IonCheckbox, IonNote } from "@ionic/react";
import { useState } from "react";
import './TaskListItem.css'

const TaskListItem = ({ task, onUpdate }: { task: any, onUpdate: (updatedTask: any) => void }) => {
  const [description, setDescription] = useState(task.description);
  const [timeoutId, setTimeoutId] = useState<NodeJS.Timeout | null>(null);
  const [completed, setCompleted] = useState(task.completed);

  const handleDescriptionChange = (e: CustomEvent<InputChangeEventDetail>) => {
    const newDescription = e.detail.value;
    setDescription(newDescription);

    if (timeoutId) {
      clearTimeout(timeoutId);
    }

    const newTimeoutId = setTimeout(() => {
      if (newDescription !== task.description) {
        onUpdate({ ...task, description: newDescription });
      }
    }, 1000);

    setTimeoutId(newTimeoutId);
  };

  const inputStyle = task.completed 
  ? { color: 'gray',textDecoration: 'line-through' } 
  : {};

  function handleCompletedChange(e: { detail: { checked: any; }; }): void {
    setCompleted(e.detail.checked);
      onUpdate({ ...task, completed: e.detail.checked });
  }

  return (
    <IonItem className="task-list-item">
      <h2>

        <div className="task-content">
          <IonCheckbox
            slot="start"
            checked={completed}
            onIonChange={handleCompletedChange} />
          <IonInput
            type="text"
            value={description}
            onIonChange={handleDescriptionChange}
            className="task-input"
            style={inputStyle}
            disabled={task.completed}
          />
          <span className="date">
            <IonNote slot="end">{task.date}</IonNote>
          </span>
        </div>

      </h2>
    </IonItem>

  );
};
export default TaskListItem;