import TaskListItem from '../components/TaskListItem';
import { useEffect, useId, useState } from 'react';
import { Task } from '../../domain/entities/Task';
import { TaskRepositoryImpl } from '../../domain/repositories/TaskRepositoryImpl';
import { IndexedDBAdapter } from '../../infra/db/IndexedDBAdapter';
import { LocalStorageAdapter } from '../../infra/db/LocalStorageAdapter';
import {
  IonButton,
  IonContent,
  IonHeader,
  IonList,
  IonPage,
  IonRefresher,
  IonRefresherContent,
  IonTitle,
  IonToolbar,
  useIonViewWillEnter
} from '@ionic/react';
import './Home.css';

const Home: React.FC = () => {
  const [tasks, setTasks] = useState<Task[]>([]);
  const taskService = new TaskRepositoryImpl(new IndexedDBAdapter());

  useEffect(() => {
    const fetchTasks = async () => {
      try {
        const tsks = await taskService.getTasks();
        setTasks(tsks);
      } catch (error) {
        console.error("Failed to fetch tasks", error);
      }
    };

    fetchTasks();
  }, []);

  useIonViewWillEnter(() => {
    const fetchTasks = async () => {
      try {
        const tsks = await taskService.getTasks();
        setTasks(tsks);
      } catch (error) {
        console.error("Failed to fetch tasks", error);
      }
    };
    fetchTasks();
  });


  const refresh = (e: CustomEvent) => {
    setTimeout(() => {
      e.detail.complete();
    }, 3000);
  };
  const LAST_ID_KEY = 'last-citation-id';
  const getNextId = (): number => {
    const lastId = parseInt(localStorage.getItem(LAST_ID_KEY) || '0', 10);
    const nextId = lastId + 1;
    localStorage.setItem(LAST_ID_KEY, nextId.toString());
    return nextId;
  };

  const handleCreateTask = async () => {
    try {
      await taskService.createTask({
        description: 'Nouvelle tâche',
        date: new Date().toLocaleDateString(),
        id: getNextId(),
        completed: false
      });

      const updatedTasks = await taskService.getTasks();
      setTasks(updatedTasks);
    } catch (error) {
      console.error("Failed to create task", error);
    }
  };
  console.log(tasks);

  return (
    <IonPage id="home-page">
      <IonHeader>
        <IonToolbar>
          <IonTitle>TodoList</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonRefresher slot="fixed" onIonRefresh={refresh}>
          <IonRefresherContent></IonRefresherContent>
        </IonRefresher>

        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">
              Todo
            </IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonList>
          {tasks.map(m => <TaskListItem key={m.id} task={m} onUpdate={(updatedTask: Task) => {
            taskService.updateTask(updatedTask).then(() => {
              const updatedTasks = tasks.map(m => m.id === updatedTask.id ? updatedTask : m);
              setTasks(updatedTasks);
            });
          }} />)}
        </IonList>
        <IonButton onClick={handleCreateTask} expand="full">Créer une tâche</IonButton>
      </IonContent>
    </IonPage>
  );
};

export default Home;

