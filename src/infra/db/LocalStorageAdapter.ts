import { ISqlAdapter } from "../../domain/interfaceAdapters/db/ISqlAdapter";
import { Task } from '../../domain/entities/Task';

export class LocalStorageAdapter implements ISqlAdapter {
    private storageKey = 'tasks';

    private readTasks(): Task[] {
        const data = localStorage.getItem(this.storageKey);
        return data ? JSON.parse(data) : [];
    }

    private writeTasks(tasks: Task[]) {
        localStorage.setItem(this.storageKey, JSON.stringify(tasks));
    }

    public async createTask(task: Task): Promise<void> {
        const tasks = this.readTasks();
        tasks.push({ ...task, id: this.getNextId() });
        this.writeTasks(tasks);
    }

    public async getTasks(): Promise<Task[]> {
        return this.readTasks();
    }

    public async updateTask(updatedTask: Task): Promise<void> {
        const tasks = this.readTasks();
        const taskIndex = tasks.findIndex(task => task.id === updatedTask.id);
        if (taskIndex !== -1) {
            tasks[taskIndex] = updatedTask;
            this.writeTasks(tasks);
        }
    }

    public async deleteTask(taskId: number): Promise<void> {
        const tasks = this.readTasks();
        const updatedTasks = tasks.filter(task => task.id !== taskId);
        this.writeTasks(updatedTasks);
    }

    private getNextId(): number {
        const tasks = this.readTasks();
        const maxId = tasks.reduce((max, task) => task.id > max ? task.id : max, 0);
        return maxId + 1;
    }
}
