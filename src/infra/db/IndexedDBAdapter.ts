import { ISqlAdapter } from '../../domain/interfaceAdapters/db/ISqlAdapter';
import { Task } from '../../domain/entities/Task';

export class IndexedDBAdapter implements ISqlAdapter {
	private db!: IDBDatabase;

	constructor() {
		this.initDB();
	}

	private initDB() {
		const request = indexedDB.open("MyDatabase", 1);

		request.onupgradeneeded = (event) => {
			const db = (event.target as IDBOpenDBRequest).result;
			if (!db.objectStoreNames.contains("tasks")) {
				db.createObjectStore("tasks", { keyPath: "id", autoIncrement: true });
			}
		};

		request.onsuccess = (event) => {
			this.db = (event.target as IDBOpenDBRequest).result;
		};

		request.onerror = (event) => {
			console.error("Database error: " + (event.target as IDBOpenDBRequest).error);
		};
	}

	public async createTask(task: Task): Promise<void> {
		const transaction = this.db.transaction(["tasks"], "readwrite");
		const store = transaction.objectStore("tasks");
		store.add(task);
	}

	public async getTasks(): Promise<Task[]> {
		return new Promise((resolve, reject) => {
			const transaction = this.db.transaction(["tasks"], "readonly");
			const store = transaction.objectStore("tasks");
			const request = store.getAll();

			request.onsuccess = () => {
				resolve(request.result);
			};

			request.onerror = () => {
				reject(request.error);
			};
		});
	}

	public async updateTask(task: Task): Promise<void> {
		const transaction = this.db.transaction(["tasks"], "readwrite");
		const store = transaction.objectStore("tasks");
		store.put(task);
	}

	public async deleteTask(taskId: number): Promise<void> {
		const transaction = this.db.transaction(["tasks"], "readwrite");
		const store = transaction.objectStore("tasks");
		store.delete(taskId);
	}
}
