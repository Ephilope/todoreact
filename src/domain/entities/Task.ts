export interface Task {
  description: string;
  date: string;
  id: number;
  completed:boolean;
}
