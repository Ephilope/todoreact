import { Task } from "../../entities/Task";
export interface ITasksRepositories {
    createTask(task: Task): void;
    getTasks(): Promise<Task[]>;
    updateTask(updatedTask: Task): void;
    deleteTask(taskId: number): void;
}