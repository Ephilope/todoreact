import { Task } from "../../entities/Task";
export interface ISqlAdapter {
    createTask(task: Task): Promise<void>;
    getTasks(): Promise<Task[]>;
    updateTask(task: Task): Promise<void>;
    deleteTask(taskId: number): Promise<void>;
}