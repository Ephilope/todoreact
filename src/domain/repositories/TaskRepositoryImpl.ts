import { Task } from "../entities/Task";
import { ITasksRepositories } from "../interfaceAdapters/repositories/ITasksRepositories";
import { ISqlAdapter } from "../interfaceAdapters/db/ISqlAdapter";

export class TaskRepositoryImpl implements ITasksRepositories {
  private tasks: Task[] = [];
  private dbAdapter: ISqlAdapter;

  constructor(dbAdapter: ISqlAdapter) {
    this.dbAdapter = dbAdapter;
  }

  public async createTask(task: Task): Promise<void> {
    await this.dbAdapter.createTask(task);
  }

  public async getTasks(): Promise<Task[]> {
    return await this.dbAdapter.getTasks();
  }

  public async updateTask(updatedTask: Task): Promise<void> {
    await this.dbAdapter.updateTask(updatedTask);
  }

  public async deleteTask(taskId: number): Promise<void> {
    await this.dbAdapter.deleteTask(taskId);
  }
}